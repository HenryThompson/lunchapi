/*
 * The Lunch Class
 */
public class Lunch {
    
    private $lunchType;
    private $lunchTime;
    private $breadAttributes;
    
    public function Lunch() {
        
    }

    public function getLunchType() {
        return $lunchType;
    }

    public function getLunchTime() {
        return $lunchTime;
    }

    public function getBreadAttributes() {
        return $breadAttributes;
    }

    public function getBreadType() {
        return $breadAttributes->getBreadType();
    }

    public function getBreadPercentage() {
        return $breadAttributes->getBreadPercentage();
    }
}
