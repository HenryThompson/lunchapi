/*
 * The LunchAPI class containing all of the important things about lunch
 * Authors: Henry Thompson, Bobby Dilley, Adam Williams
 */

require_once "lunch.php";

public class LunchAPI implements iAPI {
    
    private $lunch;

    /*
     * The lunchAPI constructor
     */
    public function lunchAPI($lunch) {
        $this->lunch = $lunch;
    }

    /*
     * Returns the JSON string to output
     */
    public function returnJSON() {
        return null;
    }
}
